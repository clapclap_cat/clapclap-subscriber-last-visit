<?php
/**
 * Plugin Name: ClapClap Subscriber Last Visit
 */
class ClapClap_Subscriber_Last_Visit {
	public static function save_user_last_visit(): void {
		update_user_meta( get_current_user_id(), 'user_last_visit', current_time( 'Y-m-d H:i:s' ) );
	}

	public static function get_user_last_visit(): DateTime|null {
		$last_visit = get_user_meta( get_current_user_id(), 'user_last_visit' );
		if ( ! isset( $last_visit ) || count( $last_visit ) < 1 ) {
			return null;
		}
		$last_visit_date = DateTime::createFromFormat( 'Y-m-d H:i:s', $last_visit[0], wp_timezone() );
		return $last_visit_date;
	}

	public static function is_date_after_user_last_visit( DateTime $date ): bool {
		$last_visit_date = self::get_user_last_visit();
		if ( ! $last_visit_date ) {
			return false;
		}
		return $date->diff( $last_visit_date )->invert === 1;
	}
}
